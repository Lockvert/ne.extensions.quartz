﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Quartz;
using Quartz.Impl;

namespace ne.extensions.quartz;

public class QuartzRunner : BackgroundService
{
    private readonly IServiceProvider _serviceProvider;

    public QuartzRunner(IServiceProvider serviceProvider)
    {
        _serviceProvider = serviceProvider;
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        var scheduler = await StdSchedulerFactory.GetDefaultScheduler(stoppingToken);
        scheduler.JobFactory = new QuartzJobFactory(_serviceProvider);

        using var scope = _serviceProvider.CreateScope();
        
        var jobs = scope.ServiceProvider.GetServices<QuartzJob>();

        foreach (var quartzJob in jobs)
        {
            var group = Guid.NewGuid().ToString();

            var fullName = quartzJob.GetType().FullName;
            if (fullName == null) continue;
            
            var detail = JobBuilder.Create(quartzJob.GetType()).WithIdentity(fullName, @group).Build();
            
            var trigger = TriggerBuilder.Create()
                .ForJob(detail).WithIdentity($"{fullName} Trigger", group)
                .WithSchedule(quartzJob.ScheduleBuilder)
                .StartAt(DateTimeOffset.Now.AddSeconds(5)).Build();
            
            await scheduler.ScheduleJob(detail, trigger, stoppingToken);
        }

        await scheduler.Start(stoppingToken);
    }
    
}