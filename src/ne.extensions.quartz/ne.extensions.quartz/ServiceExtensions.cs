﻿using System.Reflection;
using Microsoft.Extensions.DependencyInjection;
using Quartz;

namespace ne.extensions.quartz;

public static class ServiceExtensions
{
    public static IServiceCollection AddQuartsJobs(this IServiceCollection serviceCollection, params Assembly?[] assemblies)
    {
        var quartsJobs = assemblies.SelectMany(assembly => assembly?.DefinedTypes.Where(x => x.GetInterfaces().Contains(typeof(IJob))) ?? Array.Empty<TypeInfo>());
        foreach (var quartsJob in quartsJobs)
        {
            serviceCollection.AddTransient(typeof(QuartzJob), quartsJob);
        }

        serviceCollection.AddHostedService<QuartzRunner>();
        return serviceCollection;
    }
}