﻿using Microsoft.Extensions.DependencyInjection;
using Quartz;
using Quartz.Simpl;
using Quartz.Spi;

namespace ne.extensions.quartz;

public class QuartzJobFactory : SimpleJobFactory
{
    private readonly IServiceProvider _serviceProvider;

    public QuartzJobFactory(IServiceProvider serviceProvider)
    {
        _serviceProvider = serviceProvider;
    }

    public override IJob NewJob(TriggerFiredBundle bundle, IScheduler scheduler)
    {
        var localJobs = _serviceProvider.GetServices<QuartzJob>();
        var job = localJobs.FirstOrDefault(x => x.GetType().FullName == bundle.JobDetail.JobType.FullName);
        if (job is null)
            throw new Exception($"Could not find find job {bundle.JobDetail.JobType.FullName}");
        return job;
    }
}