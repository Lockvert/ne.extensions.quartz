﻿using Quartz;

namespace ne.extensions.quartz;

[DisallowConcurrentExecution]
public abstract class QuartzJob : IJob
{
    public abstract SimpleScheduleBuilder ScheduleBuilder { get; }
    public abstract Task Execute(IJobExecutionContext context);
}