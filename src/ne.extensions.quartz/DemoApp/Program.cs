using System.Reflection;
using DemoApp;
using ne.extensions.quartz;

IHost host = Host.CreateDefaultBuilder(args)
    .ConfigureServices(services =>
    {
        services.AddQuartsJobs(Assembly.GetAssembly(typeof(Worker)));
        
    }).Build();

await host.RunAsync();