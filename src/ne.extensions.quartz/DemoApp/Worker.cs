using ne.extensions.quartz;
using Quartz;

namespace DemoApp;

public class Worker : QuartzJob
{
    private readonly ILogger<Worker> _logger;
    
    public Worker(ILogger<Worker> logger)
    {
        _logger = logger;
    }

    public override SimpleScheduleBuilder ScheduleBuilder => SimpleScheduleBuilder.RepeatSecondlyForever(2);
    public override Task Execute(IJobExecutionContext context)
    {
        _logger.LogInformation("Here, doing the thing");
        return Task.CompletedTask;
    }
}