# NE.Extensions.Quartz
NE.Extensions.Quartz is a extension of the Quartz package, that implements a simple interface to make quartz jobs easy.
#


## Get Started
### NuGet 

You can run the following command to install the `ne.extensions.quartz` in your project.

```
PM> Install-Package ne.extensions.quartz
```

## Code example

Create a new Worker Service project from the .net core templates.
And add the Quartz job service like this:
```
IHost host = Host.CreateDefaultBuilder(args)
    .ConfigureServices(services =>
    {
        services.AddQuartsJobs(Assembly.GetAssembly(typeof(Worker)));
        
    }).Build();

await host.RunAsync();

```

### Quartz job implementation
Create a new class an inherit from the QuartzJob class:
```
public class Worker : QuartzJob
{
    private readonly ILogger<Worker> _logger;
    
    public Worker(ILogger<Worker> logger)
    {
        _logger = logger;
    }

    public override SimpleScheduleBuilder ScheduleBuilder => SimpleScheduleBuilder.RepeatSecondlyForever(2);
    public override Task Execute(IJobExecutionContext context)
    {
        _logger.LogInformation("Here, doing the thing");
        return Task.CompletedTask;
    }
}
```

